'use client';
import React, { useState } from 'react';

export default function Home() {
  const [result, setResult] = useState(0);
  const [num1, setNum1] = useState(0);
  const [num2, setNum2] = useState(0);
  const [op, setOp] = useState('+');

  const handleNum1Change = (e) => {
    setNum1(e.target.value);
  };

  const handleNum2Change = (e) => {
    setNum2(e.target.value);
  };

  const handleOpChange = (e) => {
    setOp(e.target.value);
  };

  const Total = () => {
    if (op === '+') {
      setResult(parseInt(num1) + parseInt(num2));
    }
    if (op === '-') {
      setResult(parseInt(num1) - parseInt(num2));
    }
    if (op === '*') {
      setResult(parseInt(num1) * parseInt(num2));
    }
    if (op === '/') {
      setResult(parseInt(num1) / parseInt(num2));
    }
  };

  return (
    <div className='flex flex-col items-center justify-center p-2 gap-5 h-screen bg-white rounded'>
      <div className='w-3/4'>
        <div className='flex w-full gap-5 justify-center'>
        <div className='flex flex-col w-1/5'><label htmlFor='num1'>Digite o primeiro numero</label>
          <input
            id='num1'
            name='num1'
            type='number'
            value={num1}
            onChange={handleNum1Change}
            className='border border-gray-300 rounded p-2 focus:outline-none focus:border-blue-500 w-full'
          />
          </div>
          <div className='w-1/8 flex items-end'>
            
            <select
              name='op'
              id='op'
              value={op}
              onChange={handleOpChange}
              className='border border-gray-300 h-[42px] rounded p-2 focus:outline-none focus:border-blue-500 w-1/8 '
            >
              <option value='+'>+</option>
              <option value='-'>-</option>
              <option value='*'>*</option>
              <option value='/'>/</option>
            </select>
          </div>
          <div className='flex flex-col w-1/5'>
            <label htmlFor='num2'>Digite o Segundo numero</label>
          <input
            id='num2'
            name='num2'
            type='number'
            value={num2}
            onChange={handleNum2Change}
            className='border border-gray-300 rounded p-2 focus:outline-none focus:border-blue-500 w-full'
          />
        </div>
        </div>

        <button
          onClick={Total}
          className='bg-red-500 w-36 rounded text-white hover:bg-red-600 focus:outline-none mt-8 mx-auto block'
        >
          Submit
        </button>
        <h1 className='font-bold text-xl text-center mt-8'>Result: {result}</h1>
      </div>
    </div>
  );
}
